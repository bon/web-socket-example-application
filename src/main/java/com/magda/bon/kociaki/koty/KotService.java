package com.magda.bon.kociaki.koty;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magda.bon.kociaki.websocket.AktualizacjaKotaEventPublisher;

/**
 * @author kba
 */
@Service
public class KotService {

	@Autowired
	private AktualizacjaKotaEventPublisher aktualizacjaKotaEventPublisher;

	private final List<Kot> koty = new ArrayList<>();

	public void dodajKota(Kot kot) {
		koty.add(kot);
	}

	public List<Kot> dajKoty() {
		return koty;
	}

	public List<String> dajOdglosyKotow() {
		return koty.stream()
				.map(kot -> kot.getOdglos())
				.collect(Collectors.toList());
	}

	public void zmienOdglos(Kot kot) {
		for (int i = 0; i < koty.size(); i++) {
			if (koty.get(i).getImie().equals(kot.getImie())) {
				koty.get(i).setOdglos(kot.getOdglos());
				aktualizacjaKotaEventPublisher.publishOrder(kot.getImie());
			}
		}
	}

	public Kot dajKota(String imieKota) {
		Kot kot = null;
		for (int i = 0; i < koty.size(); i++) {
			if (imieKota.equals(koty.get(i).getImie())) {
				kot = koty.get(i);
			}
		}
		return kot;
	}
}
