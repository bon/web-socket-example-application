package com.magda.bon.kociaki;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.magda.bon.kociaki.koty.Kot;
import com.magda.bon.kociaki.koty.KotService;

/**
 * @author kba
 */
@RestController
@RequestMapping("/koty")
public class MojRestController {

	@Autowired
	private KotService kotService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> dajKoty() {
		return ResponseEntity.ok(kotService.dajKoty());
	}

	@RequestMapping(method = RequestMethod.GET, value = "/odglosy")
	public ResponseEntity<?> dajOdglosy() {
		return ResponseEntity.ok(kotService.dajOdglosyKotow());
	}

	@RequestMapping(value = "/dodaj", method = RequestMethod.POST)
	public ResponseEntity<?> dodajKota(@RequestBody Kot kot) {
		kotService.dodajKota(kot);
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/zmien", method = RequestMethod.POST)
	public ResponseEntity<?> zmienOdglos(@RequestBody Kot kot) {
		kotService.zmienOdglos(kot);
		return ResponseEntity.ok().build();
	}
}
