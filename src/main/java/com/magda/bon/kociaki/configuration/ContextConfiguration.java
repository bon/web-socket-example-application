package com.magda.bon.kociaki.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

import lombok.extern.log4j.Log4j2;

@Configuration
@Log4j2
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.magda.bon.kociaki"}, excludeFilters = {
		@Filter(type = FilterType.ANNOTATION, value = Configuration.class)})
public class ContextConfiguration {

}