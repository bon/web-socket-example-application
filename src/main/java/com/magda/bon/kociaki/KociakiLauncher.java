package com.magda.bon.kociaki;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Import;

import com.magda.bon.kociaki.configuration.KociakiConfiguration;

/**
 * @author kba
 */
@Import(KociakiConfiguration.class)
public class KociakiLauncher {

	public static void main(String args[]) {
		SpringApplication.run(KociakiLauncher.class, args);
	}

}