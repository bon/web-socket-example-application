package com.magda.bon.kociaki.websocket;

import com.magda.bon.kociaki.koty.Kot;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author kba
 */
@Getter
@AllArgsConstructor
public class WiadomoscWebsocket extends WebSocketMessage {

	private final Kot imieKota;

}
