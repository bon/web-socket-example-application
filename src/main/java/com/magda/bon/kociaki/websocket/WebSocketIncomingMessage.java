package com.magda.bon.kociaki.websocket;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author KBa
 */
@Getter
@NoArgsConstructor
@EqualsAndHashCode
public class WebSocketIncomingMessage {

	private WebSocketIncomingAction action;

	private String imieKota;

	public void setAction(String action) {
		this.action = WebSocketIncomingAction.fromString(action.toUpperCase());
	}

	public void setKot(String kot) {
		this.imieKota = kot;
	}
}
