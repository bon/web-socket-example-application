package com.magda.bon.kociaki.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.magda.bon.kociaki.koty.KotService;

/**
 * @author kba
 */
public class InformujOZmianieOdglosuHandler extends TextWebSocketHandler implements ApplicationListener<AktualizacjaKotaEvent> {

	@Autowired
	private WebSocketActionService webSocketActionService;

	@Autowired
	private WebSocketMessageConverter webSocketMessageConverter;

	@Autowired
	private KotService kotService;

	@Override
	public void afterConnectionEstablished(WebSocketSession session) {
		webSocketActionService.addSession(session);
	}

	@Override
	public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		WebSocketIncomingMessage webSocketIncomingMessage = webSocketMessageConverter.convertToObject(message);
		WebSocketIncomingAction action = webSocketIncomingMessage.getAction();
		action.handleAction(session, webSocketIncomingMessage.getImieKota(), webSocketActionService);
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		webSocketActionService.removeSession(session);
	}

	@Override
	public void onApplicationEvent(AktualizacjaKotaEvent aktualizacjaKotaEvent) {
		String imieKota = aktualizacjaKotaEvent.getImieKota();
		try {
			webSocketActionService.sendToSession(webSocketActionService.getSesjaDlaKota(imieKota), new WiadomoscWebsocket(kotService.dajKota(imieKota)));
		} catch (SesjaDlaKotaNieIstniejeException e) {
			System.out.println("Sesja dla kota nie istnieje");
		}
	}
}
