package com.magda.bon.kociaki.websocket;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * @author KBa
 */
@Slf4j
@Component
public class WebSocketMessageConverter {

	@Autowired
	private ObjectMapper webSocketJacksonObjectMapper;

	public String convertToJson(WebSocketMessage message) {
		try {
			return webSocketJacksonObjectMapper.writeValueAsString(message);
		} catch (JsonProcessingException e) {
			log.error("Impossible to convert to JSON", e);
		}
		return null;
	}

	public WebSocketIncomingMessage convertToObject(TextMessage message) {
		try {
			return webSocketJacksonObjectMapper.readValue(message.getPayload(), WebSocketIncomingMessage.class);
		} catch (NullPointerException e) {
			log.error("Received null as TextMessage", e);
		} catch (IOException e) {
			log.error("Impossible to convert JSONObject to java object", e);
		}
		return null;
	}
}
