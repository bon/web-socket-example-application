package com.magda.bon.kociaki.websocket;

import org.springframework.context.ApplicationEvent;

import lombok.Getter;

/**
 * @author kba
 */
public class AktualizacjaKotaEvent extends ApplicationEvent {

	@Getter
	private String imieKota;

	public AktualizacjaKotaEvent(Object source, String imieKota) {
		super(source);
		this.imieKota = imieKota;
	}
}
