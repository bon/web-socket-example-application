package com.magda.bon.kociaki.websocket;

import static java.util.Collections.synchronizedSet;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import lombok.Getter;

/**
 * @author kba
 */
@Component
public class SessionHandler {

	final Set<WebSocketSession> sesje = synchronizedSet(new HashSet<WebSocketSession>());

	@Getter
	private final Map<String, WebSocketSession> sesjaDlaKota = new ConcurrentHashMap<>();

	public void addCandleSubscription(String imieKota, WebSocketSession session) {
		sesjaDlaKota.put(imieKota, session);
	}

	public void removeCandleSubscription(WebSocketSession session, String imieKota) {
		sesjaDlaKota.remove(imieKota);
		sesje.remove(session);
	}

	public void addSession(WebSocketSession session) {
		sesje.add(session);
	}

	public void removeSession(WebSocketSession session) {
		sesje.remove(session);
		sesjaDlaKota.entrySet().forEach(kotWebSocketSessionEntry -> {
			if (kotWebSocketSessionEntry.getValue().equals(session)) {
				sesjaDlaKota.remove(kotWebSocketSessionEntry.getKey());
			}
		});
	}

	public WebSocketSession getSessionForCandleForInstrument(String imieKota) {
		return sesjaDlaKota.getOrDefault(imieKota, null);
	}
}
