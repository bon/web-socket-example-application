package com.magda.bon.kociaki.websocket;

import org.springframework.web.socket.WebSocketSession;

import lombok.extern.slf4j.Slf4j;

/**
 * @author KBa
 */
@Slf4j
public enum WebSocketIncomingAction {

	DODAJ_KOTA_NASLUCHIWACZA {
		@Override
		public void handleAction(WebSocketSession session, String imieKota,
				WebSocketActionService webSocketIncomingActionService) {
			webSocketIncomingActionService.addSubscription(imieKota, session);
		}
	};

	public abstract void handleAction(WebSocketSession session, String imieKota, WebSocketActionService webSocketIncomingActionService);

	public static WebSocketIncomingAction fromString(String action) {
		return valueOf(action);
	}

}
