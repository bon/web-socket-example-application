package com.magda.bon.kociaki.websocket;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;

/**
 * @author kba
 */
@Component
public class AktualizacjaKotaEventPublisher implements ApplicationEventPublisherAware {

	private ApplicationEventPublisher orderPublisher;

	@Override
	public void setApplicationEventPublisher(ApplicationEventPublisher orderPublisher) {
		this.orderPublisher = orderPublisher;
	}

	public void publishOrder(String imieKota) {
		orderPublisher.publishEvent(new AktualizacjaKotaEvent(this, imieKota));
	}
}
