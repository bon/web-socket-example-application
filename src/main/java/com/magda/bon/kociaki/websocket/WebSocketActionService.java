package com.magda.bon.kociaki.websocket;

import java.io.IOException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

/**
 * @author kba
 */
@Service
public class WebSocketActionService {

	private final SessionHandler sessionHandler;

	private final WebSocketMessageConverter webSocketMessageConverter;

	@Autowired
	public WebSocketActionService(SessionHandler sessionHandler, WebSocketMessageConverter webSocketMessageConverter) {
		this.sessionHandler = sessionHandler;
		this.webSocketMessageConverter = webSocketMessageConverter;
	}

	public void addSession(WebSocketSession session) {
		sessionHandler.addSession(session);
	}

	public void removeSession(WebSocketSession session) {
		sessionHandler.removeSession(session);
	}

	public void addSubscription(String imieKota, WebSocketSession session) {
		if (isSubscribedCandleForSession(imieKota, session)) {
			System.out.print("Juz dodano wczesniej");
		} else {
			sessionHandler.addCandleSubscription(imieKota, session);
		}
	}

	public void sendToSession(WebSocketSession session, WebSocketMessage message) {
		try {
			session.sendMessage(new TextMessage(webSocketMessageConverter.convertToJson(message)));
		} catch (IOException ex) {
			removeSession(session);
		}
	}

	public WebSocketSession getSesjaDlaKota(String imieKota) throws SesjaDlaKotaNieIstniejeException {
		Optional<WebSocketSession> sesjaDlaKota = Optional
				.ofNullable(sessionHandler.getSessionForCandleForInstrument(imieKota));
		return sesjaDlaKota
				.orElseThrow(() -> new SesjaDlaKotaNieIstniejeException());
	}

	boolean isSubscribedCandleForSession(String imieKota, WebSocketSession session) {
		return imieKota.equals(sessionHandler.getSessionForCandleForInstrument(imieKota));
	}

}
